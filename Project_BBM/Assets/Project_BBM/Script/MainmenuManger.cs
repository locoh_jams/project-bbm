using UnityEngine;
using System.Collections;

public class MainmenuManger : MonoBehaviour {
	private UILabel text;
	
	// Use this for initialization
	void Start () {
		text = GameObject.Find("Text").GetComponent<UILabel>();
	}
	
	// Update is called once per frame
	void Update () {
		int count = StartSingleton.Instance.GetCount();
		text.text = "Count : " + count;
	}

}
