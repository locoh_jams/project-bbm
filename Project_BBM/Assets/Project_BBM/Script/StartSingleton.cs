using UnityEngine;
using System.Collections;

public class StartSingleton : MonoBehaviour {
	private int _count = 0;
    private static volatile StartSingleton instance;
		
    public static StartSingleton Instance
    {
         get 
         {
			if (instance == null) {
				// 현재 씬 내에서 MainmenuManger 컴포넌트를 검색
				instance =  FindObjectOfType(typeof (StartSingleton)) as StartSingleton;
				
			    if (instance == null) {
			    // 현재 씬에 MainmenuManger 컴포넌트가 없으면 새로 생성
					GameObject obj = new GameObject("StartSingleton");
					instance = obj.AddComponent(typeof (StartSingleton)) as StartSingleton;
				}
			}
 
            return instance;
         }
    }
	
	//종료시 컴포터넌트 삭제
	void OnApplicationQuit() {
        instance = null;
    }
	
  	public int GetCount()
    {
        //Debug.Log("Count " + _count + " times");
        return _count;
    }

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
	}
}
