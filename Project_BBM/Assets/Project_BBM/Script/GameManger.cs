using UnityEngine;
using System.Collections;

public class GameManger : MonoBehaviour {

	private int count;
	private UILabel label;
	
	// Use this for initializatio
	void Start () {
		label = GameObject.Find("count").GetComponent<UILabel>();
		count = StartSingleton.Instance.GetCount();
		//StartCoroutine(CountTime(1));
	}
	
	// Update is called once per frame
	void Update () {
		label.text = "Count : " + count;
	}
	/*
	IEnumerator CountTime(float delayTime){
		//count = MainmenuManger.Instance.Count();
		yield return new WaitForSeconds(delayTime);
		StartCoroutine(CountTime(1));
	}
	*/
}
