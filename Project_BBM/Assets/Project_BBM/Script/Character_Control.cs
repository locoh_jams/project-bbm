using UnityEngine;
using System.Collections;

public class Character_Control : MonoBehaviour {
	
	private UILabel info;
	public float moveSpeed = 1f;

	// Use this for initialization
	void Start () {
		info = GameObject.Find("info").GetComponent<UILabel>();
	}
	
	// Update is called once per frame
	void Update () {
		info.text = "transform.position.x : " + transform.position.x + 
				    "\n" + "transform.position.y : " + transform.position.y;
		MoveControl();
	}
	
	void MoveControl(){
		

		if(transform.position.x <= -2.4f || transform.position.x >= -1f 
		   || transform.position.y <= 2.01f || transform.position.y >= 3f)
        {
            float xPos = Mathf.Clamp(transform.position.x, -2.4f, -1f);
			float yPos = Mathf.Clamp(transform.position.y, 2.01f, 3f);
            transform.position = new Vector3(xPos, yPos, transform.position.z);
        }
		
		if(Input.GetKey(KeyCode.A)){ 
			transform.Translate(-1f * moveSpeed * Time.deltaTime, 0f, 0f);
		} 
		if(Input.GetKey(KeyCode.D)){ 
			transform.Translate(1f * moveSpeed * Time.deltaTime, 0f, 0f);
		} 
		if(Input.GetKey(KeyCode.S)){ 
			transform.Translate(0f, -1f * moveSpeed* Time.deltaTime, 0f);
		} 
		if(Input.GetKey(KeyCode.W)){ 
			transform.Translate(0f, 1f * moveSpeed * Time.deltaTime, 0f);
		} 
		
	}
}
